package com.example.meteoapiapp.util;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.meteoapiapp.model.WeatherReportModel;
import com.example.meteoapiapp.model.Weathers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WeatherDataService {

    public static final String REPORTS_URL = "http://82.65.60.17:25565/reports";
    Context context;

    public WeatherDataService(Context context) {
        this.context = context;
    }

    public interface ForecastResponse {
        void onError(String message);

        void onResponse(List<WeatherReportModel> weatherReportModels);
    }

    public List<WeatherReportModel> getAllReports(int radius, double latitude, double longitude, final ForecastResponse forecastResponse) {
        List<WeatherReportModel> allReports = new ArrayList<>();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, REPORTS_URL + "?radius=" + radius + "&lat=" + latitude + "&lon=" + longitude, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {

                    for (int i = 0; i < response.length(); i++) {
                        WeatherReportModel oneReport = new WeatherReportModel();
                        JSONObject weatherReport = (JSONObject) response.get(i);
                        oneReport.setId(weatherReport.getInt("id"));
                        oneReport.setLatitude(weatherReport.getDouble("latitude"));
                        oneReport.setLongitude(weatherReport.getDouble("longitude"));
                        oneReport.setTimestamp(weatherReport.getString("horodatage"));
                        oneReport.setObservedWeather(Weathers.valueOfByFrenchWeather(weatherReport.getString("temps observé")));
                        oneReport.setTemperature((float)weatherReport.getDouble("température"));
                        allReports.add(oneReport);
                    }
                    forecastResponse.onResponse(allReports);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Problem JSON", Toast.LENGTH_SHORT).show();
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);
        return allReports;
    }

    public void addReport(double latitude, double longitude, float temperature, Weathers observedWeather, final ForecastResponse forecastResponse) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("latitude", latitude);
            postData.put("longitude", longitude);
            postData.put("temps observé", observedWeather);
            postData.put("température", temperature);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, REPORTS_URL, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<WeatherReportModel> list = new ArrayList<>();
                WeatherReportModel oneReport = new WeatherReportModel();
                try {
                    oneReport.setId(response.getInt("id"));
                    oneReport.setLatitude(response.getDouble("latitude"));
                    oneReport.setLongitude(response.getDouble("longitude"));
                    oneReport.setTimestamp(response.getString("horodatage"));
                    oneReport.setObservedWeather(Weathers.valueOfByFrenchWeather(response.getString("temps observé")));
                    oneReport.setTemperature((float) response.getDouble("température"));
                    list.add(oneReport);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                forecastResponse.onResponse(list);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                forecastResponse.onError("Impossible d'ajouter un signalement.");
            }
        });

        MySingleton.getInstance(context).addToRequestQueue(request);
    }
}
