package com.example.meteoapiapp.activity;

import androidx.fragment.app.FragmentActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.example.meteoapiapp.util.GpsTracker;
import com.example.meteoapiapp.R;
import com.example.meteoapiapp.model.WeatherReportModel;
import com.example.meteoapiapp.model.Weathers;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.meteoapiapp.databinding.ActivityMapsBinding;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    List<WeatherReportModel> weatherReports;

    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        weatherReports = MainActivity.getCurrentReports();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        gpsTracker = new GpsTracker(this);
        LatLng myPos = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        mMap.addMarker(new MarkerOptions().position(myPos).title("Votre position"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myPos));
        mMap.setMinZoomPreference(10.5f);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        for (WeatherReportModel weatherReport: weatherReports) {
            LatLng latLng = new LatLng(weatherReport.getLatitude(), weatherReport.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Lat: " + weatherReport.getLatitude() + " Lon: " + weatherReport.getLongitude());
            markerOptions.icon(createSmallerBitmap(weatherReport.getObservedWeatherEnum()));
            mMap.addMarker(markerOptions);
        }
    }

    private BitmapDescriptor createSmallerBitmap(Weathers weather) {
        Bitmap b;
        switch(weather) {
            case CLOUDLESS:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_cloudless);
                break;
            case FEWCLOUDS:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_fewclouds);
                break;
            case SUNNYSPELLS:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_sunnyspells);
                break;
            case CLOUDY:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_cloudy);
                break;
            case FOGGY:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_foggy);
                break;
            case SHOWERS:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_showers);
                break;
            case RAIN:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_rain);
                break;
            case SNOW:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_snow);
                break;
            default:
                b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_unknown);
        }
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, 100, 100, false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }
}