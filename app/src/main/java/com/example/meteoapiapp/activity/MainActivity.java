package com.example.meteoapiapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meteoapiapp.adapters.WeatherSpinnerAdapter;
import com.example.meteoapiapp.model.Weathers;
import com.example.meteoapiapp.util.GpsTracker;
import com.example.meteoapiapp.R;
import com.example.meteoapiapp.util.WeatherDataService;
import com.example.meteoapiapp.adapters.WeatherReportAdapter;
import com.example.meteoapiapp.model.WeatherReportModel;
import com.google.android.gms.maps.model.LatLng;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btn_invertSort, btn_mapsView, btn_MakeReport;
    ListView lv_weatherReport;

    private SeekBar seekBar;
    private TextView textView;
    private GpsTracker gpsTracker;

    private WeatherDataService weatherDataService;


    private static LatLng currentPos;
    private static List<WeatherReportModel> currentReports;
    private boolean invertedSort;

    private final int[] mWeatherIcons = new int[] { R.drawable.ic_cloudless, R.drawable.ic_fewclouds, R.drawable.ic_sunnyspells, R.drawable.ic_cloudy,
            R.drawable.ic_foggy, R.drawable.ic_showers, R.drawable.ic_rain, R.drawable.ic_snow };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        btn_invertSort = findViewById(R.id.btn_invertSort);
        btn_mapsView = findViewById(R.id.btn_mapsView);

        this.seekBar = findViewById(R.id.seekBar);
        this.textView = findViewById(R.id.textView);

        this.seekBar.setMax(100);
        this.seekBar.setProgress(100);




        // Démarrage de l'application
        // On récupère les signalements les plus proches dans un rayon par défaut de 100 km.
        gpsTracker = new GpsTracker(MainActivity.this);
        weatherDataService = new WeatherDataService(MainActivity.this);
        getReports(100, gpsTracker.getLatitude(), gpsTracker.getLongitude(), false);
        currentPos = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

        this.textView.setText("Rayon: " + seekBar.getProgress() + "/" + seekBar.getMax() + " km");

        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView.setText("Rayon: " + seekBar.getProgress() + "/" + seekBar.getMax() + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText("Rayon: " + seekBar.getProgress() + "/" + seekBar.getMax() + " km");
                getReports(seekBar.getProgress(), gpsTracker.getLatitude(), gpsTracker.getLongitude(), invertedSort);
            }
        });

        lv_weatherReport = findViewById(R.id.lv_weatherReports);

        this.weatherDataService = new WeatherDataService(MainActivity.this);
        gpsTracker = new GpsTracker(MainActivity.this);


        btn_MakeReport = findViewById(R.id.btn_makeReport);
        btn_MakeReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = LayoutInflater.from(MainActivity.this).inflate(R.layout.make_report_dialog, null);
                Spinner weather_spinner = dialogView.findViewById(R.id.weather_spinner);
                WeatherSpinnerAdapter weatherSpinnerAdapter = new WeatherSpinnerAdapter(MainActivity.this, Arrays.asList(Weathers.values()), mWeatherIcons);
                weather_spinner.setAdapter(weatherSpinnerAdapter);
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setView(dialogView)
                        .setTitle("Effectuer un signalement")
                        .setPositiveButton("Envoyer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                EditText temperatureEditText = dialogView.findViewById(R.id.et_temperature);
                                Spinner weatherSpinner = dialogView.findViewById(R.id.weather_spinner);
                                float temperature = Float.parseFloat(temperatureEditText.getText().toString());
                                Weathers weather = (Weathers)weatherSpinner.getSelectedItem();
                                weatherDataService.addReport(gpsTracker.getLatitude(), gpsTracker.getLongitude(), temperature, weather, new WeatherDataService.ForecastResponse() {
                                    @Override
                                    public void onError(String message) {
                                        Toast.makeText(MainActivity.this, "Impossible de contacter l'API.", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onResponse(List<WeatherReportModel> weatherReportModels) {
                                        getReports(seekBar.getProgress(), gpsTracker.getLatitude(), gpsTracker.getLongitude(), invertedSort);
                                    }
                                });
                            }
                        })
                        .setNegativeButton("Annuler", null)
                        .create();
                dialog.show();
            }
        });

        btn_invertSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!invertedSort) {
                    getReports(seekBar.getProgress(), gpsTracker.getLatitude(), gpsTracker.getLongitude(), true);
                    invertedSort = true;
                    btn_invertSort.setText("Rétablir");
                } else {
                    getReports(seekBar.getProgress(), gpsTracker.getLatitude(), gpsTracker.getLongitude(), false);
                    invertedSort = false;
                    btn_invertSort.setText("Inverser tri");
                }
            }
        });

        btn_mapsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(i);
            }
        });
    }

    public static LatLng getCurrentPos() {
        return currentPos;
    }
    public static List<WeatherReportModel> getCurrentReports() {
        return currentReports;
    }

    private void getReports(int radius, double latitude, double longitude, boolean inverted) {
        weatherDataService.getAllReports(radius, gpsTracker.getLatitude(), gpsTracker.getLongitude(), new WeatherDataService.ForecastResponse() {
            @Override
            public void onError(String message) {
                Toast.makeText(MainActivity.this, "Impossible de contacter l'API.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(List<WeatherReportModel> weatherReportModels) {
                // On réduit la liste à 10 signalements si besoin
                int k = weatherReportModels.size();
                if ( k > 10 ) weatherReportModels.subList(10, k).clear();

                // Comparateur en fonction de la distance
                Comparator<WeatherReportModel> distanceComparator = new Comparator<WeatherReportModel>() {
                    @Override
                    public int compare(WeatherReportModel report1, WeatherReportModel report2) {
                        double distance1 = WeatherReportModel.computeDistance(report1.getLatitude(), latitude, report1.getLongitude(), longitude, 0, 0);
                        double distance2 = WeatherReportModel.computeDistance(report2.getLatitude(), latitude, report2.getLongitude(), longitude, 0, 0);
                        if (!inverted) {
                            return Double.compare(distance1, distance2);
                        }
                        return Double.compare(distance2, distance1);
                    }
                };

                // Comparateur en fonction de l'horodatage
                Comparator<WeatherReportModel> timestampComparator = new Comparator<WeatherReportModel>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public int compare(WeatherReportModel report1, WeatherReportModel report2) {
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                        LocalDateTime timestamp1 = LocalDateTime.parse(report1.getTimestamp(), formatter);
                        LocalDateTime timestamp2 = LocalDateTime.parse(report2.getTimestamp(), formatter);
                        if (!inverted) {
                            return timestamp2.compareTo(timestamp1);
                        }
                        return timestamp1.compareTo(timestamp2);
                    }
                };
                Comparator<WeatherReportModel> compositeComparator = distanceComparator.thenComparing(timestampComparator);
                Collections.sort(weatherReportModels, compositeComparator);

                WeatherReportAdapter weatherReportAdapter = new WeatherReportAdapter(MainActivity.this, weatherReportModels);
                lv_weatherReport.setAdapter(weatherReportAdapter);
                currentReports = weatherReportModels;
            }
        });

    }
}