package com.example.meteoapiapp.model;

public class WeatherReportModel {
    private long id;

    private String timestamp;

    private double latitude;

    private double longitude;

    private Weathers observedWeather;

    private float temperature;

    public WeatherReportModel() {}

    public WeatherReportModel(String timestamp, float latitude, float longitude, Weathers observedWeather, float temperature) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.observedWeather = observedWeather;
        this.temperature = temperature;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getObservedWeather() {
        return observedWeather.toLocalizedString();
    }
    public Weathers getObservedWeatherEnum() {
        return observedWeather;
    }

    public void setObservedWeather(Weathers observedWeather) {
        this.observedWeather = observedWeather;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Horodatage : " + timestamp +
                " Latitude : " + latitude +
                " Longitude : " + longitude +
                " Temps observé : " + observedWeather.toLocalizedString() +
                " Température : " + temperature;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    public static double computeDistance(double lat1, double lat2, double lon1,
                                         double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
