package com.example.meteoapiapp.model;

public enum Weathers {
    CLOUDLESS("Ciel bleu"),
    FEWCLOUDS("Quelques nuages"),
    SUNNYSPELLS("Eclaircies"),
    CLOUDY("Couvert"),
    FOGGY("Brouillard"),
    SHOWERS("Averses"),
    RAIN("Pluie"),
    SNOW("Neige");

    private final String localizedString;

    Weathers(String localizedString) {
        this.localizedString = localizedString;
    }

    public String toLocalizedString() {
        return this.localizedString;
    }

    public static Weathers valueOfByFrenchWeather(String frenchWeather) throws IllegalArgumentException {
        for (Weathers value : values()) {
            String weather = value.toLocalizedString();
            if (weather.equalsIgnoreCase(frenchWeather)) {
                return value;
            }
        }
        throw new IllegalArgumentException("Weather " + frenchWeather + " does not exist!");
    }
}
