package com.example.meteoapiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.meteoapiapp.R;
import com.example.meteoapiapp.model.Weathers;

import java.util.List;

public class WeatherSpinnerAdapter extends ArrayAdapter<Weathers> {

    private Context mContext;
    private List<Weathers> mWeatherList;
    private int[] mWeatherIcons;

    public WeatherSpinnerAdapter(Context context, List<Weathers> weatherList, int[] weatherIcons) {
        super(context, 0, weatherList);
        mContext = context;
        mWeatherList = weatherList;
        mWeatherIcons = weatherIcons;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_spinner_item, parent, false);
        }
        Weathers weather = mWeatherList.get(position);
        ImageView ivWeatherIcon = convertView.findViewById(R.id.iv_spinner_icon);
        TextView tvSpinnerText = convertView.findViewById(R.id.tv_spinner_text);
        ivWeatherIcon.setImageResource(mWeatherIcons[weather.ordinal()]);
        tvSpinnerText.setText(Weathers.values()[weather.ordinal()].toLocalizedString());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_spinner_item, parent, false);
        }

        Weathers weather = mWeatherList.get(position);
        ImageView ivWeatherIcon = convertView.findViewById(R.id.iv_spinner_icon);
        TextView tvSpinnerText = convertView.findViewById(R.id.tv_spinner_text);
        ivWeatherIcon.setImageResource(mWeatherIcons[weather.ordinal()]);
        tvSpinnerText.setText(Weathers.values()[weather.ordinal()].toLocalizedString());

        return convertView;
    }
}
