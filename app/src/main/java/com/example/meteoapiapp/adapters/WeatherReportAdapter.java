package com.example.meteoapiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.meteoapiapp.R;
import com.example.meteoapiapp.model.WeatherReportModel;
import com.example.meteoapiapp.model.Weathers;

import java.util.List;

public class WeatherReportAdapter extends ArrayAdapter<WeatherReportModel> {
    private Context context;

    public WeatherReportAdapter(Context context, List<WeatherReportModel> weatherReports) {
        super(context, 0, weatherReports);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WeatherReportModel weatherReport = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_list_item, parent, false);
        }

        TextView tvTemperature = convertView.findViewById(R.id.tv_temperature);
        TextView tvTimestamp = convertView.findViewById(R.id.tv_timestamp);
        ImageView ivWeatherIcon = convertView.findViewById(R.id.iv_weather_icon);
        ivWeatherIcon.setMaxHeight(10);
        ivWeatherIcon.setMaxWidth(10);

        tvTemperature.setText(weatherReport.getTemperature() + "°");
        tvTimestamp.setText(weatherReport.getTimestamp());

        switch (Weathers.valueOfByFrenchWeather(weatherReport.getObservedWeather())) {
            case CLOUDLESS:
                ivWeatherIcon.setImageResource(R.drawable.ic_cloudless);
                break;
            case FEWCLOUDS:
                ivWeatherIcon.setImageResource(R.drawable.ic_fewclouds);
                break;
            case SUNNYSPELLS:
                ivWeatherIcon.setImageResource(R.drawable.ic_sunnyspells);
                break;
            case CLOUDY:
                ivWeatherIcon.setImageResource(R.drawable.ic_cloudy);
                break;
            case FOGGY:
                ivWeatherIcon.setImageResource(R.drawable.ic_foggy);
                break;
            case SHOWERS:
                ivWeatherIcon.setImageResource(R.drawable.ic_showers);
                break;
            case RAIN:
                ivWeatherIcon.setImageResource(R.drawable.ic_rain);
                break;
            case SNOW:
                ivWeatherIcon.setImageResource(R.drawable.ic_snow);
                break;
            default:
                ivWeatherIcon.setImageResource(R.drawable.ic_unknown);
                break;
        }

        return convertView;
    }
}